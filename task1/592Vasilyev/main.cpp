#include <iostream>
#include <sstream>
#include <vector>

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

class SampleApplication : public Application 
{
public:
    MeshPtr _paraboloid;

    ShaderProgramPtr _shader;

    float _N = 50.0;
    float _radius = 2.0;

    void makeScene() override
    {
        Application::makeScene();

        //Создаем пораболоид
        _paraboloid = makeParaboloid(_radius, static_cast<unsigned int>(_N));
        _paraboloid->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("592VasilyevData1/shader.vert", "592VasilyevData1/shader.frag");

    }

    void update() override
    {
        Application::update();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        const double multiplier = 0.5;

        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS) {
                _N = _N - 5.0f;
                if (_N < 5.0) {
                    _N = 5.0;
                }
                _paraboloid = makeParaboloid(_radius, static_cast<unsigned int>(_N));
            }
            if (key == GLFW_KEY_EQUAL) {
                _N = _N + 5.0f;
                _paraboloid = makeParaboloid(_radius, static_cast<unsigned int>(_N));
            }
        }

        update();
    }

    void draw() override {
         Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Передаем время в шейдер
        _shader->setFloatUniform("time", (float)glfwGetTime()); 

        //Общие переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем меш
        _shader->setMat4Uniform("modelMatrix", _paraboloid->modelMatrix());
        _paraboloid->draw();
    }
};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}