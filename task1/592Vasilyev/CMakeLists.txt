set(SRC_FILES
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    main.cpp
)

set(HEADER_FILES
    common/Application.hpp
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

set(SHADER_FILES
    592Vasilyev/shader.vert
    592Vasilyev/shader.frag
)

source_group("Shaders" FILES
    ${SHADER_FILES}
)

include_directories(common)

MAKE_OPENGL_TASK(592Vasilyev 1 "${SRC_FILES}")